from django.test import TestCase
from users.models import User


class TestModels(TestCase):
    def setUp(self):
        self.user1 = User.objects.create_user(
            username='P245',
            password='Pitu4django'
        )
        self.user2 = User .objects.create_superuser(
            username='Tincho',
            password='locoloco123'
        )

    def test_users_is_create(self):
        self.assertEquals(self.user1.username, 'P245')
        self.assertEquals(self.user1.is_active, True)
        self.assertEquals(self.user1.is_staff, False)
        self.assertEquals(self.user1.is_superuser, False)
        self.assertEquals(self.user1.check_password('Pitu4django'), True)

    def test_users_create_superuser(self):
        self.assertEquals(self.user2.username, 'Tincho')
        self.assertEquals(self.user2.is_active, True)
        self.assertEquals(self.user2.is_staff, True)
        self.assertEquals(self.user2.is_superuser, True)
        self.assertEquals(self.user2.check_password('locoloco123'), True)

    def test_user_security_password(self):
        self.assertFalse(self.user1.check_password('Nopassword1'))
        self.assertNotEquals(self.user1.password, 'Pitu4django')
