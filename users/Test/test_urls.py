from django.test import SimpleTestCase, Client
from django.urls import reverse, resolve
from rest_framework import status
from users.views import LoginView


class TestUrls(SimpleTestCase):
    def test_url_is_resolved(self):
        url = reverse('login')
        self.assertEquals(resolve(url).func.view_class, LoginView)

    def test_url_undefined_method(self):
        client = Client()
        response = client.get(reverse('login'))
        self.assertEquals(
            response.status_code,
            status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_url_not_found(self):
        client = Client()
        response = client.get('logon/')
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)
