from django.test import TestCase, Client
from django.urls import reverse
from users.models import User
from rest_framework.authtoken.models import Token
from rest_framework import status
import json


class TestViews(TestCase):

    def setUp(self):
        self.login_url = reverse('login')
        self.client = Client()
        self.user1 = User.objects.create_user(
            username='P245', password='Djangotest1')
        self.register_url = reverse('register')

    def test_login_user_active_POST(self):
        data = {
            'user': 'P245',
            'pass': 'Djangotest1'
        }
        response = self.client.post(self.login_url, data)
        token, created = Token.objects.get_or_create(user=self.user1)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(created, False)
        self.assertEquals(response.json()['token'], token.key)

    def test_login_no_data_POST(self):
        response = self.client.post(self.login_url)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(
            response.json(),
            {'detail': 'Invalid input'})

    def test_login_data_no_valid_POST(self):
        data = {
            'user': 'P245',
            'pass': 'djangotest1'
        }
        response = self.client.post(self.login_url, data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(response.json(), ['Unable to login.'])

    def test_login_user_inactive_POST(self):
        data = {
            'user': 'P245',
            'pass': 'Djangotest1'
        }
        self.user1.is_active = False
        self.user1.save()
        response = self.client.post(self.login_url, data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(response.json(), ['Unable to login.'])

    def test_login_no_pass_POST(self):
        data = {
            'user': 'P245'
        }
        response = self.client.post(self.login_url, data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(
            response.json(),
            {'detail': 'Invalid input'})

    def test_login_no_user_POST(self):
        data = {
            'pass': 'django123'
        }
        response = self.client.post(self.login_url, data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(
            response.json(),
            {'detail': 'Invalid input'})

    def test_register_user_POST(self):
        data = {
            'user': 'P246',
            'pass': 'Djangotest1'
        }
        response = self.client.post(self.register_url, data=data)
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_register_exist_username_POST(self):
        data = {
            'user': 'P245',
            'pass': 'Djangotest1'
        }
        response = self.client.post(self.register_url, data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(response.json(), ['this username alredy exist.'])

    def test_register_no_data_POST(self):
        response = self.client.post(self.register_url)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(
            response.json(),
            {'detail': 'Invalid input'})

    def test_register_no_user_POST(self):
        data = {
            'pass': 'Djangotest1'
        }
        response = self.client.post(self.register_url, data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(
            response.json(),
            {'detail': 'Invalid input'})

    def test_register_no_pass_POST(self):
        data = {
            'user': 'Djangotest1'
        }
        response = self.client.post(self.register_url, data=data)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEquals(
            response.json(),
            {'detail': 'Invalid input'})
