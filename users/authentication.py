from datetime import timedelta
from django.conf import settings
from django.utils import timezone
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework import exceptions
import datetime
from django.utils.timezone import utc

EXPIRE_HOURS = getattr(settings, 'REST_FRAMEWORK_TOKEN_EXPIRE_HOURS', 8)


class ExpiringTokenAuthentication(TokenAuthentication):
    model = Token

    def authenticate_credentials(self, key):
        try:
            token = self.model.objects.get(key=key)
        except self.model.DoesNotExist:
            raise exceptions.AuthenticationFailed('Invalid token')
        if not token.user.is_active:
            raise exceptions.AuthenticationFailed('User inactive or deleted')
        if token.created < timezone.now() - timedelta(hours=EXPIRE_HOURS):
            token.delete()
            raise exceptions.AuthenticationFailed('Token has expired:')
        token.created = datetime.datetime.utcnow().replace(tzinfo=utc)
        token.save()
        return (token.user, token)
