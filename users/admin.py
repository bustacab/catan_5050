from django.contrib import admin
from users.models import User
from django.contrib.auth.admin import UserAdmin


class PersonalizedUserAdmin(UserAdmin):
    fieldsets = ()
    add_fieldsets = (
        (None, {
            'fields': ('username', 'password1', 'password2'),
        }),
    )
    list_display = ('username', 'is_active', 'is_staff',)
    search_fields = ('username',)


admin.site.register(User, PersonalizedUserAdmin)
