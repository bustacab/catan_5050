from rest_framework.views import APIView
from django.contrib.auth import login as django_login, authenticate
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import exceptions
from rest_framework import status
from django.utils import timezone
from django.utils.timezone import utc
from users.models import User
import datetime


class LoginView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        username = request.data.get('user', '')
        password = request.data.get('pass', '')
        if len(request.data) != 2:
            msg = "Invalid input"
            raise exceptions.ParseError(msg)

        if password and username:
            pass
        else:
            msg = 'the username or password is not found in the request.'
            raise exceptions.ValidationError(msg)
        user = authenticate(username=username, password=password)
        if user:
            django_login(request, user)
            token, created = Token.objects.get_or_create(user=user)
            if not created:
                token.created = datetime.datetime.utcnow().replace(tzinfo=utc)
                token.save()
            return Response({'token': token.key}, status=status.HTTP_200_OK)
        else:
            msg = 'Unable to login.'
            raise exceptions.ValidationError(msg)


class RegisterView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        username = request.data.get('user', '')
        password = request.data.get('pass', '')
        if len(request.data) != 2:
            msg = "Invalid input"
            raise exceptions.ParseError(msg)
        if password and username:
            pass
        else:
            msg = 'the username or password is not found in the request.'
            raise exceptions.ValidationError(msg)
        users = User.objects.filter(username=username)
        if len(users) != 0:
            msg = 'this username alredy exist.'
            raise exceptions.ValidationError(msg)
        User.objects.create_user(username=username, password=password)
        return Response(status=status.HTTP_204_NO_CONTENT)
