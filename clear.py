#!usr/bin/python

import subprocess
import shlex
import os
import sys


def clear():

    if sys.platform == "win32":
        command_line = 'del /S "__pycache__"'
        os.system(command_line)

        command_line1 = 'del /S "000*.py"'
        os.system(command_line1)

        command_line2 = 'del /S "db.sqlite3"'
        os.system(command_line2)

    else:
        command_line = r'find . -name "__pycache__" -exec rm -r {} \;'
        args = shlex.split(command_line)
        subprocess.call(args)

        command_line1 = r'find . -name "000*.py" -exec rm -r {} \;'
        args1 = shlex.split(command_line1)
        subprocess.call(args1)

        command_line2 = r'find . -name "db.sqlite3" -exec rm -r {} \;'
        args2 = shlex.split(command_line2)
        subprocess.call(args2)


def load():

    if sys.platform == "win32":

        os.system("python manage.py makemigrations")
        os.system("python manage.py migrate")

    else:
        command_line3 = 'python manage.py makemigrations'
        args3 = shlex.split(command_line3)
        subprocess.call(args3)
        command_line4 = 'python manage.py migrate'
        args4 = shlex.split(command_line4)
        subprocess.call(args4)


def main():
    print("Press 1 to clear database")
    print("Press 2 to clear and load database")
    print("Press 3 to load database")
    key = input()

    if key == "1":
        clear()
    elif key == "2":
        clear()
        load()
    elif key == "3":
        load()
    else:
        print("It is not a valid keyboard input, run the script again")


if __name__ == '__main__':
    main()
