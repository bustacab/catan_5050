from django.test import TestCase, Client
from django.urls import reverse, resolve
from rest_framework import status
from room.views import RoomViewSet
from room.models import Room
from users.models import User


class TestUrls(TestCase):

    def test_url_is_resolved(self):
        url = "/rooms/"
        self.assertEqual(
            resolve(url).func.__name__,
            RoomViewSet.as_view({'get': 'list'}).__name__
        )
