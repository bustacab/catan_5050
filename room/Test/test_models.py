from django.test import TestCase
from users.models import User
from room.models import Room


class TestModels(TestCase):

    def setUp(self):
        self.login_url = 'http://localhost:8000/users/login/'
        self.user = User.objects.create_user(
            username='Nahu',
            password='admin'
        )
        self.room = Room.objects.create(
            name='test',
            owner=self.user,
            max_players=1
        )
        self.user1 = User.objects.create_user(
            username='Guest',
            password='guest'
        )
        data = {
            'user': 'Nahu',
            'pass': 'admin'
        }
        data1 = {
            'user': 'Guest',
            'pass': 'guest'
        }

    def testRoomIsCreated(self):
        self.assertEquals(self.room.id, 1)
        self.assertEquals(self.room.name, 'test')

    def testUserJoinedRoom(self):
        self.room.players.add(self.user)
        self.assertEquals(len(self.room.players.all()), 1)
        self.assertEquals(self.room.name, 'test')
