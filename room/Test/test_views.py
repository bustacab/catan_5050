from django.test import TestCase, Client
from django.urls import reverse
from users.models import User
from room.models import Room
from tablero.models import Board, VertexPosition, Hexagon
from rest_framework import status
from rest_framework.authtoken.models import Token
import json


def add_players_to_room(room, players):
    for player in players:
        room.players.add(player)
    room.save()


class TestViews(TestCase):

    def setUp(self):
        self.login_url = 'http://localhost:8000/users/login/'
        self.client = Client()
        self.board = Board.objects.create(name='tessst')
        self.vertex = VertexPosition.objects.create(level=0, index=0)
        self.hexagon = Hexagon.objects.create(
            resource='TERRAIN',
            board=self.board,
            vertex_position=self.vertex
        )
        self.hexagon.vertices.add(self.vertex)
        self.user = User.objects.create_user(
            username='Nahu',
            password='admin'
        )
        self.room = Room.objects.create(
            name='test',
            owner=self.user,
            max_players=1,
            board_id=self.board
        )
        self.user1 = User.objects.create_user(
            username='Guest',
            password='guest'
        )
        self.user2 = User.objects.create_user(
            username='Test',
            password='test'
        )

        data = {
            'user': 'Nahu',
            'pass': 'admin'
        }
        data1 = {
            'user': 'Guest',
            'pass': 'guest'
        }
        data2 = {
            'user': 'Test',
            'pass': 'test'
        }
        response_nahu = self.client.post(self.login_url, data)
        response_guest = self.client.post(self.login_url, data1)
        response_test = self.client.post(self.login_url, data2)
        self.token_nahu = response_nahu.json()['token']
        self.token_guest = response_guest.json()['token']
        self.token_test = response_test.json()['token']

    def test_room_list_GET(self):
        room_list_url = 'http://localhost:8000/rooms/'
        response = self.client.get(
            room_list_url, HTTP_AUTHORIZATION='Token ' + self.token_nahu)
        expected_answer = [{
            "id": 1,
            "name": "test",
            "owner": "Nahu",
            "players": [],
            "max_players": 1,
            "game_has_started": False,
            "game_id": None

        }]

        self.assertEquals(expected_answer, response.json())

    def test_room_join_PUT(self):
        room_join_url = 'http://localhost:8000/rooms/1/'
        response = self.client.put(
            room_join_url, HTTP_AUTHORIZATION='Token ' + self.token_nahu)

        self.assertEquals(response.status_code, status.HTTP_200_OK)

    def test_room_join_fail_PUT(self):
        room_join_url = 'http://localhost:8000/rooms/1/'
        response = self.client.put(
            room_join_url, HTTP_AUTHORIZATION='Token ' + self.token_nahu)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        response1 = self.client.put(
            room_join_url, HTTP_AUTHORIZATION='Token ' + self.token_guest)

        self.assertEquals(response1.status_code, status.HTTP_400_BAD_REQUEST)

    def test_room_create_POST(self):
        url = 'http://localhost:8000/rooms/'
        data = {"name": "test2", "board_id": 1}

        http_auth = 'Token ' + self.token_guest

        response = self.client.post(path=url,
                                    data=data, HTTP_AUTHORIZATION=http_auth)
        expected_answer = {
            "id": 2,
            "name": "test2",
            "owner": "Guest",
            "players": [],
            "max_players": 3,
            "game_has_started": False,
            "game_id": None
        }
        self.assertEquals(expected_answer, response.json())

    def test_room_create_fail_POST(self):
        url = 'http://localhost:8000/rooms/'
        data = {"name": "test1", "board_id": 1}
        http_auth = 'Token ' + self.token_guest
        response = self.client.post(path=url,
                                    data=data, HTTP_AUTHORIZATION=http_auth)
        self.assertEquals(status.HTTP_200_OK, response.status_code)
        data["name"] = "test3"
        http_auth = 'Token ' + self.token_guest
        response = self.client.post(path=url,
                                    data=data, HTTP_AUTHORIZATION=http_auth)

        self.assertEquals(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_room_game_start_PATCH(self):
        url = 'http://localhost:8000/rooms/1/'
        self.room.max_players = 4
        add_players_to_room(
            self.room,
            players=[
                self.user,
                self.user1,
                self.user2])

        http_auth = 'Token ' + self.token_nahu
        response = self.client.patch(url, HTTP_AUTHORIZATION=http_auth)
        self.assertEquals(status.HTTP_200_OK, response.status_code)
        check_url = 'http://localhost:8000/rooms/1/'
        expected_answer = {
            'id': 1,
            'name': 'test',
            'owner': 'Nahu',
            'players': ['Nahu', 'Guest', 'Test'],
            'max_players': 4,
            'game_has_started': True,
            'game_id': 1
        }
        self.assertEquals(
            self.client.get(
                check_url,
                HTTP_AUTHORIZATION=http_auth).data,
            expected_answer)

    def test_room_game_start_fail_PATCH(self):
        url = 'http://localhost:8000/rooms/1/'
        self.room.max_players = 4

        http_auth = 'Token ' + self.token_nahu
        response = self.client.patch(url, HTTP_AUTHORIZATION=http_auth)
        self.assertEquals(status.HTTP_403_FORBIDDEN, response.status_code)
        add_players_to_room(
            self.room,
            players=[
                self.user,
                self.user1,
                self.user2])
        http_auth = 'Token ' + self.token_guest
        response = self.client.patch(url, HTTP_AUTHORIZATION=http_auth)
        self.assertEquals(status.HTTP_403_FORBIDDEN, response.status_code)

        http_auth = 'Token ' + self.token_nahu
        response = self.client.patch(url, HTTP_AUTHORIZATION=http_auth)
        self.assertEquals(status.HTTP_200_OK, response.status_code)

    def test_room_cancel_DELETE(self):
        url = 'http://localhost:8000/rooms/1/'

        http_auth = 'Token ' + self.token_nahu

        self.room.owner = self.user1
        self.room.save()

        response = self.client.delete(url, HTTP_AUTHORIZATION=http_auth)
        self.assertEquals(status.HTTP_403_FORBIDDEN, response.status_code)

        self.room.owner = self.user
        self.room.save()

        self.room.game_has_started = True
        self.room.save()

        response = self.client.delete(url, HTTP_AUTHORIZATION=http_auth)
        self.assertEquals(status.HTTP_409_CONFLICT, response.status_code)

        self.room.game_has_started = False
        self.room.save()

        response = self.client.delete(url, HTTP_AUTHORIZATION=http_auth)
        self.assertEquals(status.HTTP_204_NO_CONTENT, response.status_code)

        response = self.client.delete(url, HTTP_AUTHORIZATION=http_auth)
        self.assertEquals(status.HTTP_404_NOT_FOUND, response.status_code)
