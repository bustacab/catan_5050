from rest_framework.serializers import ModelSerializer

from users.models import User
from users.serializers import UserSerializer
from room.models import Room
from game.serializers import GameSerializer


class PlayerSerializer(UserSerializer):

    def to_representation(self, user):
        return user.username


class RoomSerializer(ModelSerializer):

    players = PlayerSerializer(many=True, read_only=True)
    owner = PlayerSerializer(read_only=True)
    game_id = GameSerializer(read_only=True)
    game_id = game_id["id"]

    class Meta:

        model = Room
        fields = (
            'id', 'name', 'owner', 'players', 'max_players',
            'game_has_started',
            'game_id'
        )
