from django.urls import path
from room.views import RoomViewSet
from django.conf.urls import include, re_path
from rest_framework import routers


router = routers.DefaultRouter()
router.register(prefix=r'', viewset=RoomViewSet, base_name='rooms')

urlpatterns = [
    re_path('^', include(router.urls)),
]
