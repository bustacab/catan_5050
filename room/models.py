from django.db import models
from users.models import User
from game.models import Game
from tablero.models import Board

# Create your models here.


class Room(models.Model):
    name = models.CharField(max_length=30)
    owner = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        null=False
    )
    players = models.ManyToManyField(User, related_name='rooms')
    game_id = models.OneToOneField(
        Game,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    board_id = models.ForeignKey(
        Board,
        null=True,
        related_name='board',
        on_delete=models.CASCADE
    )
    max_players = models.IntegerField(default=3)
    game_has_started = models.BooleanField(default=False)

    def __str__(self):
        players = ''
        for user in self.players.all():
            players += user.username + " "
        return self.name + " " + self.owner.username \
            + " " + players + " " + str(self.max_players)
