from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import Room
from users.models import User
from .serializers import RoomSerializer
from django.core.exceptions import SuspiciousOperation, ObjectDoesNotExist
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import exceptions
from django.forms.models import model_to_dict
from game.models import Player, Game
from tablero.models import Board, VertexPosition, Hexagon
from catan.choices import valid_input
import json
import random


class RoomViewSet(ModelViewSet):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer

    def update(self, request, pk):
        # Join Room
        room = self.get_object()
        joined_players = room.players.count()
        max_players = room.max_players
        if joined_players < max_players:
            logged_usr = User.objects.get(pk=request.user.id)
            room.players.add(logged_usr)
            room.save()
            if joined_players < room.players.count():
                return Response("You have joined the room.", status=200)
            else:
                raise SuspiciousOperation('The user is already in the room.')

        else:
            raise SuspiciousOperation('Failed to join: Room is full.')

    def create(self, request):
        # check if user already has a room
        if len(request.data) == 2:
            valid_input(
                request.data, (request.data.get(
                    'name', None) is None or request.data.get(
                    'board_id', None) is None))
        else:
            msg = 'Invalid input'
            raise exceptions.ParseError(msg)
        try:
            u = User.objects.get(pk=request.user.id).room

            return Response(
                "The user is already the owner of a room.",
                status=403
            )
        except ObjectDoesNotExist:
            pass
        try:
            r = Room.objects.get(name=request.data['name'])

            return Response(
                "Room name already exist.",
                status=403
            )
        except ObjectDoesNotExist:
            r = Room.objects.create(
                name=request.data['name'],
                owner=request.user,
                board_id=Board.objects.get(
                    pk=request.data['board_id']))
            r.save()
            return Response(RoomSerializer(r).data)

    def partial_update(self, request, pk):
        room = self.get_object()
        if room.players.count() < 3:
            return Response(
                "There's not enough players ",
                status=403
            )
        elif room.game_has_started:
            return Response(
                "Game is already started",
                status=403
            )
        elif room.owner.username != request.user.username:
            return Response(
                "You're not allowed to start the game.",
                status=403
            )

        g = Game.objects.create(
            name=room.name,
            board=room.board_id,
            robber=Hexagon.objects.get(
                vertex_position=VertexPosition.objects.get(
                    level=0,
                    index=0)))
        room.game_has_started = True
        room.save()
        amount_of_players = len(room.players.all())
        list_of_turns = random.sample(
            range(0, amount_of_players), amount_of_players)

        colour = ['red', 'green', 'blue', 'yellow']
        for i, player in enumerate(room.players.all(), start=0):
            usr_player = Player.objects.create(user=player)
            usr_player.colour = colour[i]
            usr_player.turn_number = list_of_turns[i]
            usr_player.save()
            g.players.add(usr_player)
            if list_of_turns[i] == 0:
                g.in_turn = usr_player

        g.save()
        room.game_id = g

        room.save()
        return Response("The game has started.", status=200)

    def destroy(self, request, pk):
        if request.user != self.get_object().owner:
            return Response(
                "Forbidden action for this user",
                status=403
            )
        elif self.get_object().game_has_started:
            return Response(
                "The room is in game",
                status=409
            )
        else:
            return super().destroy(request, pk)
