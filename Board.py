from django.contrib.admin.models import LogEntry
from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.sessions.models import Session
from rest_framework.authtoken.models import Token
from room.models import Room
from tablero.models import Hexagon, Board, VertexPosition
from game.models import Card, Game, Player, Resource, RoadPosition, Town
from users.models import User
# Shell Plus Django Imports
from django.core.cache import cache
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.models import (
    Avg,
    Case,
    Count,
    F,
    Max,
    Min,
    Prefetch,
    Q,
    Sum,
    When,
    Exists,
    OuterRef,
    Subquery
)
from django.utils import timezone
from django.urls import reverse
from random import SystemRandom, choice
import random
import os
import sys
import subprocess
import shlex


def dbRemake_byOs(os_label):
    print("""\n\n\n""")
    print("""\n\n\n""")
    print("""The operative system is:""", os_label)
    print("""\n\n\n""")
    print("""\n\n\n""")
    if os_label == "win32":
        command_line = 'del /S "__pycache__"'
        os.system(command_line)
        command_line1 = 'del /S "000*.py"'
        os.system(command_line1)
        command_line2 = 'del /S "db.sqlite3"'
        os.system(command_line2)
        os.system("python manage.py makemigrations")
        os.system("python manage.py migrate")
    else:
        command_line = r'find . -name "__pycache__" -exec rm -r {} \;'
        args = shlex.split(command_line)
        subprocess.call(args)
        command_line1 = r'find . -name "000*.py" -exec rm -r {} \;'
        args1 = shlex.split(command_line1)
        subprocess.call(args1)
        command_line2 = r'find . -name "db.sqlite3" -exec rm -r {} \;'
        args2 = shlex.split(command_line2)
        subprocess.call(args2)
        command_line3 = 'python manage.py makemigrations'
        args3 = shlex.split(command_line3)
        subprocess.call(args3)
        command_line4 = 'python manage.py migrate'
        args4 = shlex.split(command_line4)
        subprocess.call(args4)


board = Board.objects.create(
    name="Tablero Demo"
)

Terrain = ['brick', 'ore', 'lumber', 'wool', 'grain']

dice_possible_results = list(range(2, 13))
dice_possible_results.remove(7)

for level in range(3):
    if level == 0:
        r = 1
    elif level == 1:
        r = 6
    else:
        r = 12
    for index in range(r):
        if level == 0 and index == 0:
            resource = 'desert'
        else:
            resource = Terrain[
                random.randint(0,
                               len(Terrain) - 1)
            ]
        ver = VertexPosition.objects.get_or_create(
            level=level,
            index=index
        )
        hexa = Hexagon.objects.get_or_create(
            resource=resource,
            board=board,
            vertex_position=ver[0],
            token=random.choice(dice_possible_results)
        )

for level in range(3):
    if level == 0:
        r = 6
    elif level == 1:
        r = 18
    else:
        r = 30
    for index in range(r):
        ver = VertexPosition.objects.get_or_create(
            level=level,
            index=index
        )

matrix = []
level0 = [[x for x in VertexPosition.objects.all() if x.level == 0]]
level1 = []
level2 = []
i = 0
h = 0
for j, k in zip(range(6), range(6)):
    j = i
    k = (i / 3)
    i += 3
    level1.append([x for x in VertexPosition.objects.all() if (
        x.level == 1 and j <= x.index <= i) or (
        x.level == 0 and k <= x.index <= (i / 3))])
# final cases
level1[5].append(VertexPosition.objects.get(level=0, index=0))
level1[5].append(VertexPosition.objects.get(level=1, index=0))

# base case
level2.append([x for x in VertexPosition.objects.all()
               if x.level == 2 and (0 <= x.index <= 1 or x.index == 29)])
level2[0] += [x for x in VertexPosition.objects.all() if x.level ==
              1 and (0 <= x.index <= 1 or x.index == 17)]
i = 1
h = 1
three = True
for j, k in zip(range(1, 12), range(1, 12)):
    j = i
    k = h
    if three:
        i += 3
        h += 1
    else:
        i += 2
        h += 2
    level2.append([x for x in VertexPosition.objects.all() if (
        x.level == 2 and j <= x.index <= i) or (
        x.level == 1 and k <= x.index <= h)])
    three = not three

# definition of all vertices for each hexagon
matrix = level0 + level1 + level2

# filling database
i = 0
j = 0
for level in range(3):
    if level == 0:
        r = 1
    elif level == 1:
        r = 6
    else:
        r = 12
    for index in range(r):
        ver = VertexPosition.objects.get(
            level=level,
            index=index
        )

        hexa = Hexagon.objects.get(
            vertex_position=ver
        )
        for j in range(6):
            hexa.vertices.add(matrix[i][j])
        i += 1
