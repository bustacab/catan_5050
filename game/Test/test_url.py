from django.urls import reverse, resolve
from django.test import TestCase
from game.views import ResourceAndCardsAPI, ActionsAPI


class UrlTest(TestCase):
    def test_url_resolve1(self):
        url = reverse("game-id:player", kwargs={'id': 1})
        self.assertEquals(resolve(url).func.view_class, ResourceAndCardsAPI)

    def test_url_resolve2(self):
        url = reverse("game-id:playerActions", kwargs={'id': 1})
        self.assertEquals(resolve(url).func.view_class, ActionsAPI)
