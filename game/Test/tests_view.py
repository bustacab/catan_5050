from rest_framework import status
from rest_framework.authtoken.models import Token
from django.urls import reverse
from django.test import TestCase, Client
from users.models import User
from game.models import (
    Game,
    Resource,
    Card,
    Player,
    Town,
    RoadPosition
)
from room.models import Room
from tablero.models import Hexagon, VertexPosition, Board
from catan.choices import TERRAIN
import random
import json
import datetime


class CardsResourcesTestCase(TestCase):

    def setUp(self):
        # ***************************************************
        # users and tokens
        # user1 -> token1
        # user2 -> token2
        # user3 -> token3
        # user4 -> token4
        # user5 -> token5
        self.user1 = User.objects.create_user(
            username='P245', password='Djangotest1')
        self.user2 = User.objects.create_user(
            username='tincho', password='Djangotest1')
        self.user3 = User.objects.create_user(
            username='enturno', password='Djangotest1')
        self.user4 = User.objects.create_user(
            username='banco', password='Djangotest1')
        self.user5 = User.objects.create_user(
            username='not in turn', password='Djangotest1')
        self.user6 = User.objects.create_user(
            username='nur', password='Djangotest1')
        self.user10 = User.objects.create_user(
            username='P300', password='Djangotest1')
        self.token1 = Token.objects.create(user=self.user1)
        self.token2 = Token.objects.create(user=self.user2)
        self.token3 = Token.objects.create(user=self.user3)
        self.token4 = Token.objects.create(user=self.user4)
        self.token5 = Token.objects.create(user=self.user5)
        self.token6 = Token.objects.create(user=self.user6)
        self.token10 = Token.objects.create(user=self.user10)
        self.client = Client()
        # ***************************************************

        self.room = Room.objects.create(
            name='test',
            owner=self.user1,
            max_players=3
        )

        # ***************************************************
        # player1 -> user1
        # player2 -> user2
        # player3 -> user3
        # player4 -> user4
        # player5 -> user5
        self.player1 = Player.objects.create(colour='green',
                                             user=self.user1
                                             )

        self.player2 = Player.objects.create(colour='blue',
                                             user=self.user2
                                             )

        self.player3 = Player.objects.create(colour='black',
                                             user=self.user3
                                             )

        self.player4 = Player.objects.create(colour='black',
                                             user=self.user4
                                             )

        self.player5 = Player.objects.create(colour='red',
                                             user=self.user5
                                             )

        self.player6 = Player.objects.create(colour='red',
                                             user=self.user6
                                             )
        self.player10 = Player.objects.create(colour='green',
                                              user=self.user10
                                              )
        # ***************************************************
        # board
        board = Board.objects.create(
            pk=10,
            name="Board Example"
        )

        # ***************************************************
        # game1 -> [player1), player2, player3(in_turn), player5]
        # game2 -> [player6(in_turn)]
        # game3 -> [player4(in_turn)]
        # game4 -> []

        self.game1 = Game.objects.create(id=1,
                                         name='gam',
                                         in_turn=self.player3,
                                         board=board)

        self.game1.players.add(self.player1)
        self.game1.players.add(self.player2)

        self.game2 = Game.objects.create(id=2,
                                         name='gam',
                                         in_turn=self.player6,
                                         board=board)

        self.game2.players.add(self.player6)
        self.game1.players.add(self.player3)
        self.game1.players.add(self.player5)
        self.game3 = Game.objects.create(id=3,
                                         name='gam',
                                         in_turn=self.player4,
                                         board=board)

        self.game3.players.add(self.player4)

        self.game4 = Game.objects.create(id=4,
                                         name='gam',
                                         in_turn=self.player4,
                                         board=board)

        self.game4.players.add(self.player4)

        # ***************************************************

        # ***************************************************

        resource1 = Resource.objects.create(player=self.player1,
                                            resource_type='wool')

        resource2 = Resource.objects.create(player=self.player1,
                                            resource_type='ore')

        resource3 = Resource.objects.create(player=self.player2,
                                            resource_type='wool')
        for x in range(5):
            resource = Resource.objects.create(player=self.player4,
                                               resource_type='lumber')

        resource4 = Resource.objects.create(player=self.player4,
                                            resource_type='ore')

        resource5 = Resource.objects.create(player=self.player3,
                                            resource_type='ore')

        resource6 = Resource.objects.create(player=self.player3,
                                            resource_type='wool')

        resource7 = Resource.objects.create(player=self.player3,
                                            resource_type='grain')

        card1 = Card.objects.create(player=self.player1,
                                    card_type='road_building')

        card2 = Card.objects.create(player=self.player1,
                                    card_type='monopoly')

        card3 = Card.objects.create(player=self.player2,
                                    card_type='road_building')

        card4 = Card.objects.create(player=self.player10,
                                    card_type='knight')
        # ***************************************************
        # Test for Buy a card
        self.Terrain = ['brick', 'ore', 'lumber', 'wool', 'grain', 'desert']

        self.game5 = Game.objects.create(id=10,
                                         name='gam',
                                         in_turn=self.player10,
                                         board=board)

        self.game5.players.add(self.player10)
        self.game5.players.add(self.player6)
        for level in range(3):
            if level == 0:
                r = 1
            elif level == 1:
                r = 6
            else:
                r = 12
            for index in range(r):
                if level == 0 and index == 0:
                    resource = 'desert'
                else:
                    resource = self.Terrain[
                        random.randint(0,
                                       len(self.Terrain) - 1)
                    ]
                ver = VertexPosition.objects.get_or_create(
                    level=level,
                    index=index
                )
                hexa = Hexagon.objects.get_or_create(
                    resource=resource,
                    board=board,
                    vertex_position=ver[0],
                    token=random.randint(2, 12)
                )
                if level == 0 and index == 0:
                    self.game5.robber = hexa[0]
                    hexa[0].token = 0
                    hexa[0].save()
                    self.game5.save()

        hex1 = Hexagon.objects.get(
            vertex_position=VertexPosition.objects.get(
                level=1, index=0))
        hex1.resource = "wool"
        hex1.token = 6
        hex1.save()
        hex2 = Hexagon.objects.get(
            vertex_position=VertexPosition.objects.get(
                level=1, index=1))
        hex2.resource = "ore"
        hex2.token = 6
        hex2.save()

        for level in range(3):
            if level == 0:
                r = 6
            elif level == 1:
                r = 18
            else:
                r = 30
            for index in range(r):
                ver = VertexPosition.objects.get_or_create(
                    level=level,
                    index=index
                )
                if level == 0 and index == 1:
                    Town.objects.get_or_create(
                        owner=self.player10, vertex_position=ver[0])

    def test_cards_resources(self):
        dataPlayer1 = {
            'resources': ['wool', 'ore'],
            'cards': ['road_building', 'monopoly']
        }
        dataPlayer2 = {
            'resources': ['wool'],
            'cards': ['road_building']
        }
        dataEmpty = {
            'resources': [],
            'cards': []
        }
        url = reverse("game-id:player", kwargs={'id': 1})
        response1 = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token1.key)
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(response1.json(), dataPlayer1)
        response2 = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token2.key)
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.json(), dataPlayer2)
        url = reverse("game-id:player", kwargs={'id': 2})
        response3 = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token6.key)
        self.assertEqual(response3.status_code, status.HTTP_200_OK)
        self.assertEqual(response3.json(), dataEmpty)

    def test_id_not_found(self):
        url = 'http://localhost:8000/games/123123/players/'
        response = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token1.key)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_id_as_string(self):
        response = self.client.get('game/hola/player')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_invalid_token(self):
        url = reverse("game-id:player", kwargs={'id': 1})
        response = self.client.get(
            url, HTTP_AUTHORIZATION='Token ')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_id_not_found(self):
        url = reverse("game-id:player", kwargs={'id': 1})
        self.token1.created -= datetime.timedelta(hours=9)
        self.token1.save()
        response = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token1.key)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_not_accept_bank_trade(self):
        action = {
            "type": "bank_trade",
            "payload": {"give": "brick", "receive": "lumber"}
        }
        url = reverse("game-id:playerActions", kwargs={'id': 4})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token4.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)

    def test_accept_bank_trade(self):
        dataPlayer1 = {
            'resources': [
                'lumber',
                'lumber',
                'lumber',
                'lumber',
                'lumber',
                'ore'],
            'cards': []}
        dataPlayer2 = {
            'resources': ['lumber', 'ore', 'brick'],
            'cards': []
        }
        action = {
            "type": "bank_trade",
            "payload": {"give": "lumber", "receive": "brick"}
        }

        url = reverse("game-id:player", kwargs={'id': 4})
        response = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token4.key)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), dataPlayer1)

        url = reverse("game-id:playerActions", kwargs={'id': 4})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token4.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_200_OK)

        url = reverse("game-id:player", kwargs={'id': 4})
        response2 = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token4.key)
        self.assertEqual(response2.status_code, status.HTTP_200_OK)
        self.assertEqual(response2.json(), dataPlayer2)

    def test_give_same_receive(self):
        action = {
            "type": "bank_trade",
            "payload": {"give": "lumber", "receive": "lumber"}
        }

        url = reverse("game-id:playerActions", kwargs={'id': 4})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token4.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)

    def test_game_not_exists(self):
        action = {
            "type": "bank_trade",
            "payload": {"give": "lumber", "receive": "lumber"}
        }
        url = reverse("game-id:playerActions", kwargs={'id': 123213})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token4.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_404_NOT_FOUND)

    def test_player_not_in_game(self):
        action = {
            "type": "bank_trade",
            "payload": {"give": "lumber", "receive": "lumber"}
        }
        url = reverse("game-id:playerActions", kwargs={'id': 1})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token4.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_404_NOT_FOUND)

    def test_action_bad_request_type(self):
        action = {
            "type": "water",
            "payload": {"give": "lumber", "receive": "lumber"}
        }
        url = reverse("game-id:playerActions", kwargs={'id': 4})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token4.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_400_BAD_REQUEST)

    def test_action_bad_request_payload_not_resources(self):
        action = {
            "type": "bank_trade",
            "payload": {"give": "water", "receive": "lumber"}
        }
        url = reverse("game-id:playerActions", kwargs={'id': 4})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token4.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_400_BAD_REQUEST)

    def test_action_bad_request_payload_not_resources_and_not_type(self):
        action = {
            "type": "water",
            "payload": {"give": "water", "receive": "lumber"}
        }
        url = reverse("game-id:playerActions", kwargs={'id': 4})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token4.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_400_BAD_REQUEST)

    def test_buy_a_card(self):
        dataPlayer1 = {
            'resources': ['ore', 'wool', 'grain'],
            'cards': []
        }
        action = {
            "type": "buy_card",
        }

        url = reverse("game-id:player", kwargs={'id': 1})
        response = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token3.key)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), dataPlayer1)

        url = reverse("game-id:playerActions", kwargs={'id': 1})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token3.key,
            content_type="application/json")
        resCount = len(self.game1.player_resources(
            user=self.user3))
        cardCount = len(self.game1.player_cards(
            user=self.user3))
        self.assertEqual(response1.status_code, status.HTTP_200_OK)
        self.assertEqual(resCount, 0)
        self.assertEqual(cardCount, 1)

    def test_buy_a_card_not_in_turn(self):
        dataPlayer1 = {
            'resources': [],
            'cards': []
        }
        action = {
            "type": "buy_card"
        }

        url = reverse("game-id:player", kwargs={'id': 1})
        response = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token5.key)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), dataPlayer1)

        url = reverse("game-id:playerActions", kwargs={'id': 1})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token5.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)

    def test_game_status(self):
        url = f'http://localhost:8000/games/{self.game5.id}/'
        response = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token5.key)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_game_status_not_game(self):
        url = 'http://localhost:8000/games/123123/'
        response = self.client.get(
            url, HTTP_AUTHORIZATION='Token ' + self.token5.key)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_build_a_road_in_a_valid_position(self):
        action = {
            "type": "build_road",
                    "payload": [{
                                "level": "0",
                                "index": "1"
                                },
                                {
                                "level": "1",
                                "index": "3"
                                }]
        }

        self.game5.turn = 10
        self.game5.save()

        Resource.objects.create(player=self.player10,
                                resource_type='brick')

        Resource.objects.create(player=self.player10,
                                resource_type='lumber')

        RoadPosition.objects.get_or_create(
            pos1=VertexPosition.objects.get(
                level=0, index=1), pos2=VertexPosition.objects.get(
                level=0, index=0), owner=self.player10)

        RoadPosition.objects.get_or_create(
            pos1=VertexPosition.objects.get(
                level=0, index=1), pos2=VertexPosition.objects.get(
                level=0, index=2), owner=self.player10)

        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_204_NO_CONTENT)

    def test_build_a_road_in_a_bad_position(self):
        action = {
            "type": "build_road",
                    "payload": [{
                                "level": "2",
                                "index": "2"
                                },
                                {
                                "level": "2",
                                "index": "3"
                                }]
        }

        Resource.objects.create(player=self.player10,
                                resource_type='brick')

        Resource.objects.create(player=self.player10,
                                resource_type='lumber')

        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_400_BAD_REQUEST)

    def test_build_a_road_without_resources(self):
        action = {
            "type": "build_road",
                    "payload": [{
                                "level": "1",
                                "index": "2"
                                },
                                {
                                "level": "1",
                                "index": "3"
                                }]
        }
        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_400_BAD_REQUEST)

    def test_play_a_building_road_card_with_valid_positions(self):
        action = {
            "type": "play_road_building_card",
            "payload": [
                [{
                    "level": "0",
                    "index": "1"
                },
                    {
                    "level": "0",
                    "index": "2"
                }],
                [{
                    "level": "0",
                    "index": "2"
                },
                    {
                    "level": "0",
                    "index": "3"
                }]
            ]
        }
        Card.objects.create(player=self.player10,
                            card_type='road_building')

        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_204_NO_CONTENT)

    def test_play_a_building_road_card_with_no_valid_positions(self):
        action = {
            "type": "play_road_building_card",
            "payload": [
                [{
                    "level": "2",
                    "index": "1"
                },
                    {
                    "level": "2",
                    "index": "2"
                }],
                [{
                    "level": "2",
                    "index": "2"
                },
                    {
                    "level": "2",
                    "index": "3"
                }]
            ]
        }
        Card.objects.create(player=self.player10,
                            card_type='road_building')

        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)

    def test_play_a_building_road_card_with_no_card(self):
        action = {
            "type": "play_road_building_card",
            "payload": [
                [{
                    "level": "0",
                    "index": "1"
                },
                    {
                    "level": "0",
                    "index": "2"
                }],
                [{
                    "level": "0",
                    "index": "2"
                },
                    {
                    "level": "0",
                    "index": "3"
                }]
            ]
        }

        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)

    def test_action_get(self):
        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.get(
            url,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_200_OK)

    def test_distribute_resources(self):
        before_matched_token = len(
            Resource.objects.filter(
                player=self.player10))
        self.game5.turn = 8
        while self.game5.dice1 + self.game5.dice2 != 6:
            if self.game5.in_turn == self.player1:
                self.game5.in_turn = self.player2
                self.game5.save()
            else:
                self.game5.in_turn = self.player1
                self.game5.save()
        after_matched_token = len(
            Resource.objects.filter(
                player=self.player10))
        self.assertEqual(before_matched_token, after_matched_token)

    def test_end_turn(self):
        url = 'http://localhost:8000/rooms/1/'
        url1 = reverse("game-id:playerActions", kwargs={'id': 11})
        action = {
            "type": "end_turn",
            "payload": []
        }
        self.room.max_players = 3
        self.room.players.add(self.user1)
        self.room.players.add(self.user2)
        self.room.players.add(self.user3)
        response = self.client.patch(
            url, HTTP_AUTHORIZATION='Token ' + self.token1.key)
        game11 = Game.objects.get(id=11)
        round1 = [player for player in game11.players.all()]
        round1.sort(key=lambda x: x.turn_number)
        round2 = round1[::-1]
        round3 = list(round1)
        round4 = list(round1)
        turn_order = round1 + round2 + round3 + round4
        for i in range(len(turn_order) - 1):
            if game11.in_turn.user.username == "P245":
                token = self.token1
            elif game11.in_turn.user.username == "tincho":
                token = self.token2
            else:
                token = self.token3
            Town.objects.create(
                vertex_position=VertexPosition.objects.get(
                    level=2, index=i), owner=game11.in_turn)
            RoadPosition.objects.create(
                pos1=VertexPosition.objects.get(
                    level=2, index=i), pos2=VertexPosition.objects.get(
                    level=2, index=i + 1), owner=game11.in_turn)
            response1 = self.client.post(
                url1, action, HTTP_AUTHORIZATION='Token ' + token.key)
            game11 = Game.objects.get(id=11)
            self.assertEqual(response1.status_code, status.HTTP_204_NO_CONTENT)
            self.assertEqual(turn_order[i + 1], game11.in_turn)

    def test_move_robber_with_play_knight_successfully(self):
        self.game5.turn = 10
        self.game5.in_turn = self.player10
        self.game5.save()

        action = {
            "type": "play_knight_card",
                    "payload": {
                        "position": {
                            "level": 1,
                            "index": 0
                        },
                        "player": ""
                    }
        }
        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_204_NO_CONTENT)

    def test_move_robber_with_play_knight_not_successfully(self):
        self.game5.turn = 10
        self.game5.in_turn = self.player10
        self.game5.save()
        action = {
            "type": "play_knight_card",
                    "payload": {
                        "position": {
                            "level": 0,
                            "index": 0
                        },
                        "player": ""
                    }
        }

        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)

    def test_move_robber_with_play_knight_invalid_player_to_steal(self):
        self.game5.turn = 10
        self.game5.in_turn = self.player10
        self.game5.save()

        action = {
            "type": "play_knight_card",
                    "payload": {
                        "position": {
                            "level": 1,
                            "index": 0
                        },
                        "player": ['Water']
                    }
        }
        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)

    def test_move_robber_with_play_knight_players_cant_steal_themselves(self):
        self.game5.turn = 10
        self.game5.in_turn = self.player10
        self.game5.save()

        action = {
            "type": "play_knight_card",
                    "payload": {
                        "position": {
                            "level": 1,
                            "index": 0
                        },
                        "player": [self.player10.user.username]
                    }
        }
        url = reverse("game-id:playerActions", kwargs={'id': 10})
        response1 = self.client.post(
            url,
            action,
            HTTP_AUTHORIZATION='Token ' +
            self.token10.key,
            content_type="application/json")
        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)

    def test_end_turn_didnt_build_town_in_first_turn(self):
        url = 'http://localhost:8000/rooms/1/'
        url1 = reverse("game-id:playerActions", kwargs={'id': 11})
        action = {
            "type": "end_turn",
            "payload": []
        }
        self.room.max_players = 3
        self.room.players.add(self.user1)
        self.room.players.add(self.user2)
        self.room.players.add(self.user3)
        response = self.client.patch(
            url, HTTP_AUTHORIZATION='Token ' + self.token1.key)
        game11 = Game.objects.get(id=11)
        if game11.in_turn.user.username == "P245":
            token = self.token1
        elif game11.in_turn.user.username == "tincho":
            token = self.token2
        else:
            token = self.token3
        response1 = self.client.post(
            url1, action, HTTP_AUTHORIZATION='Token ' + token.key)
        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)

    def test_end_game_endpoints(self):
        for i in range(0, 11):
            Card.objects.create(player=self.player3,
                                card_type='victory_point')

        self.game1.save()
        # Game.objects.get(pk=1).save()

        response = self.client.get(
            'http://localhost:8000/games/1/board',
            HTTP_AUTHORIZATION='Token ' + self.token3.key
        )

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        response1 = self.client.get(
            'http://localhost:8000/games/1/player/actions/',
            HTTP_AUTHORIZATION='Token ' + self.token3.key
        )

        self.assertEqual(response1.status_code, status.HTTP_403_FORBIDDEN)
