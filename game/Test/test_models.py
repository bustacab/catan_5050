from django.urls import reverse
from rest_framework.test import APITestCase
from users.models import User
from game.models import Game, Resource, Card, Player
import math


class CardsResourcesModelTest(APITestCase):

    def setUp(self):
        self.user1 = User.objects.create_user(
            username='P245',
            password='Pitu4django'
        )

        self.user2 = User.objects.create_user(
            username='P246',
            password='Pitu4django'
        )

        self.user3 = User.objects.create_user(
            username='enturno',
            password='Djangotest1'
        )

        self.player1 = Player.objects.create(colour='green',
                                             user=self.user1
                                             )

        self.player2 = Player.objects.create(colour='green',
                                             user=self.user2
                                             )

        self.player3 = Player.objects.create(colour='yellow',
                                             user=self.user3
                                             )

        self.game = Game.objects.create(id=1,
                                        name='gam',
                                        in_turn=self.player1
                                        )

        self.game3 = Game.objects.create(id=3,
                                         name='gam',
                                         in_turn=self.player3)

        self.game3.players.add(self.player1)
        self.game3.players.add(self.player2)
        self.game3.players.add(self.player3)

        self.resource1 = Resource.objects.create(player=self.player1,
                                                 resource_type='wool')

        self.resource2 = Resource.objects.create(player=self.player1,
                                                 resource_type='ore')

        self.card1 = Card.objects.create(player=self.player1,
                                         card_type='road_building')

        self.card2 = Card.objects.create(player=self.player1,
                                         card_type='monopoly')

# **********************************************************************
        # Roll 7 resources for test
        # player3 =
        # Resources[wool, ore, brick, ore, brick,
        # wool, lumber, grain, grain, lumber, ore]
        resources_list = [
            'wool',
            'ore',
            'brick',
            'ore',
            'brick',
            'wool',
            'lumber',
            'grain',
            'grain',
            'lumber',
            'ore']
        for resource in resources_list:
            self.resources = Resource.objects.create(player=self.player3,
                                                     resource_type=resource)

    def testPlayerIsCreated(self):
        self.assertEquals(self.player1.colour, 'green')

    def testGameIsCreated(self):
        self.assertEquals(self.game.id, 1)
        self.assertEquals(self.game.name, 'gam')

    def testResourcesAreCreated(self):
        self.assertEquals(self.resource1.resource_type, 'wool')
        self.assertEquals(self.resource2.resource_type, 'ore')

    def testCardsAreCreated(self):
        self.assertEquals(self.card1.card_type, 'road_building')
        self.assertEquals(self.card2.card_type, 'monopoly')

    def testRollDice(self):
        dice1 = self.game.dice1
        dice2 = self.game.dice2
        count = 0
        self.game.turn = 8
        for i in range(200):
            if self.game.in_turn == self.player1:
                self.game.in_turn = self.player2
                self.game.save()
            else:
                self.game.in_turn = self.player1
                self.game.save()
            if dice1 != self.game.dice1 or dice2 != self.game.dice2:
                count += 1
            dice1 = self.game.dice1
            dice2 = self.game.dice2
        self.assertGreaterEqual(count, 100)

    def testDiceRolls7(self):
        resourcesCount = len(
            [x for x in Resource.objects.filter(player=self.player3)])
        dice1 = self.game3.dice1
        dice2 = self.game3.dice2
        self.game3.turn = 8
        diceResult = 0
        while not diceResult == 7:
            if self.game3.in_turn == self.player1:
                self.game3.in_turn = self.player2
                self.game3.save()
            elif self.game3.in_turn == self.player2:
                self.game3.in_turn = self.player3
                self.game3.save()
            else:
                self.game3.in_turn = self.player1
                self.game3.save()
            dice1 = self.game3.dice1
            dice2 = self.game3.dice2
            diceResult = dice1 + dice2
        newResourcesCount = len(
            [x for x in Resource.objects.filter(player=self.player3)])
        resourcesCount -= math.trunc(resourcesCount / 2)
        self.assertEqual(resourcesCount, newResourcesCount)

    def test_vp_added(self):
        card4 = Card.objects.create(player=self.player3,
                                    card_type='victory_point')
        card5 = Card.objects.create(player=self.player3,
                                    card_type='victory_point')
        Card.objects.create(player=self.player3,
                            card_type='victory_point')

        g = card4.player.player_game.first()

        player3 = g.players.filter(user=self.player3.user).first()

        self.assertEqual(player3.victory_points, 3)

    def player_crowned(self):
        Card.objects.create(player=self.player3,
                            card_type='victory_point')
        Card.objects.create(player=self.player3,
                            card_type='victory_point')
        Card.objects.create(player=self.player3,
                            card_type='victory_point')
        Card.objects.create(player=self.player3,
                            card_type='victory_point')
        Card.objects.create(player=self.player3,
                            card_type='victory_point')
        Card.objects.create(player=self.player3,
                            card_type='victory_point')
        Card.objects.create(player=self.player3,
                            card_type='victory_point')
        Card.objects.create(player=self.player3,
                            card_type='victory_point')
        Card.objects.create(player=self.player3,
                            card_type='victory_point')
        Card.objects.create(player=self.player3,
                            card_type='victory_point')

        g = card4.player.player_game.first()

        player3 = g.players.filter(user=self.player3.user).first()

        self.assertEqual(player3.victory_points, 10)
        self.assertEqual(g.winner, player3)
