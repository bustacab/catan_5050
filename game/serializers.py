from rest_framework import serializers
from game.models import Card, Resource, Game, Town


class ResourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resource
        fields = [('resource_type')]


class CardsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = [('card_type')]


class TownSerializer(serializers.ModelSerializer):
    class Meta:
        model = Town
        field = "__all__"


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = "__all__"


class GameListSerializer(serializers.ModelSerializer):
    in_turn = serializers.CharField(
        read_only=True, source='in_turn.user.username')

    class Meta:
        model = Game
        fields = ['id', 'name', 'in_turn']
