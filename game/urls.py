from django.urls import path
from game.views import ResourceAndCardsAPI, ActionsAPI, GameViewSet
from django.conf.urls import include, re_path
from rest_framework import routers

app_name = 'game'

router = routers.DefaultRouter()
router.register(prefix=r'', viewset=GameViewSet, base_name='games')

urlpatterns = [
    re_path(
        '^',
        include(
            router.urls)),
    path(
        '<int:id>/player/',
        ResourceAndCardsAPI.as_view(),
        name='player'),
    path(
        '<int:id>/player/actions/',
        ActionsAPI.as_view(),
        name='playerActions')]
