from .serializers import GameListSerializer
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from django.shortcuts import render
from game.serializers import (
    ResourceSerializer,
    CardsSerializer,
    GameSerializer,
)
from tablero.serializers import (
    HexagonSerializer,
    VertexPositionSerializer
)
from game.models import (Resource, Card, Game,
                         RoadPosition, Player, Town, VertexPosition)
from game.endgame import is_game_ended
from tablero.models import Board
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework import status, exceptions
from django.http import Http404
from django.shortcuts import get_object_or_404
from .adjacency import adjacency
from catan.choices import valid_input
import random
import json
import math
import traceback


def get_settlements_available(game):
    vertexs = VertexPosition.objects.all()
    available_vertexs = []
    for vertex in vertexs:
        if check_neighbors(game=game, level=vertex.level,
                           index=vertex.index):
            available_vertexs.append(vertex)

    return available_vertexs


def check_neighbors(game, level, index):
    level, index = int(level), int(index)

    neighbors_pos = adjacency[(level, index)]
    towns = []
    roads = []
    owner_roads = []
    exist_something = False
    for player in game.players.all():
        exist_something = exist_something or player.check_my_towns(
            level, index)
    if exist_something:
        return False

    for neighbor_pos in neighbors_pos:

        for player in game.players.all():
            town = player.get_my_town(neighbor_pos[0],
                                      neighbor_pos[1])
            if town:
                towns.append(town)
            road = player.get_my_road(neighbor_pos[0],
                                      neighbor_pos[1], level, index)

            road2 = player.get_my_road(level, index, neighbor_pos[0],
                                       neighbor_pos[1])

            if road:
                roads.append(road)
            if road2:
                roads.append(road2)

    for road in roads:
        if road.owner != game.in_turn:
            return False
        else:
            owner_roads.append(road)
    if towns:
        return False
    if len(game.in_turn.town_set.all()) < 2:
        return True
    if not owner_roads:
        return False
    return True


def is_vertex(json):
    a = json.get("level", None)
    b = json.get("index", None)
    if a is not None and b is not None:
        return True
    else:
        return False


class ResourceAndCardsAPI(APIView):
    def convert(self, list):
        listAux = []
        for elemento in list:
            value = elemento.values()
            listAux.extend(value)
        return listAux

    def get(self, request, id, format=None):
        if is_game_ended(id):
            return is_game_ended(id)
        try:
            filteredGame = Game.objects.get(id=id)
            playerGame = filteredGame.players.get(user_id=request.user.id)
        except ObjectDoesNotExist:
            raise Http404

        resources = filteredGame.player_resources(user=request.user.id)
        cards = filteredGame.player_cards(user=request.user.id)
        responseResource = ResourceSerializer(resources, many=True)
        responseCards = CardsSerializer(cards, many=True)
        return Response(
            {
                'resources': self.convert(responseResource.data),
                'cards': self.convert(responseCards.data)
            },
            status=status.HTTP_200_OK
        )


class ActionsAPI(APIView):

    def parseObjectError(self, error):
        if str(error) == "Game matching query does not exist.":
            return "Game id does not exist."
        elif str(error) == "Player matching query does not exist.":
            return "player does not exist in game."

    def post(self, request, id):
        if is_game_ended(id):
            return is_game_ended(id)
        try:
            filteredGame = Game.objects.get(id=id)
            player = filteredGame.players.get(user_id=request.user.id)
        except ObjectDoesNotExist as e:
            error = self.parseObjectError(e)
            raise exceptions.NotFound(error)

        if player != filteredGame.in_turn:
            msg = "player must be in turn to perform this action"
            raise exceptions.PermissionDenied(msg)

        # Remember to add actions to list when implementing them
        valid_actions = [
            'buy_card',
            'bank_trade',
            'build_road',
            'play_road_building_card',
            'play_knight_card',
            'move_robber']
        body = request.data
        for req in body:
            if req != 'type' and req != 'payload':
                msg = 'Invalid input'
                raise exceptions.ParseError(msg)
        action = body.get('type', None)
        payload = body.get('payload', None)
        resourcePlayer = filteredGame.player_resources(user=request.user.id)

        if action == 'buy_card':
            return filteredGame.buy_card(resourcePlayer)

        elif action == 'bank_trade':
            valid_input(
                payload, (payload.get(
                    'give', None) is None or payload.get(
                    'receive', None) is None))
            return filteredGame.bank_trade(payload, resourcePlayer)

        elif action == 'end_turn':
            return filteredGame.end_turn()

        elif action == 'build_road':
            if len(payload) == 2:
                for pay in payload:
                    if len(pay) == 2:
                        valid_input(
                            pay, (pay.get(
                                'level', None) is None or pay.get(
                                'index', None) is None))
                    else:
                        msg = 'Invalid input'
                        raise exceptions.ParseError(msg)
            else:
                msg = 'Invalid input'
                raise exceptions.ParseError(msg)

            msg = {'detail': 'the payload not contain a vertex'}
            if is_vertex(payload[0]) and is_vertex(payload[1]):
                if player.check_availability(
                        payload[0], payload[1], filteredGame):
                    resourcePlayer = resourcePlayer
                    resource_type_list = [
                        x.resource_type for x in resourcePlayer]
                    road_buy_resource_list = ['lumber', 'brick']
                    if len(player.roads.all()) >= 2:
                        if set(road_buy_resource_list).issubset(
                                resource_type_list):
                            resourcePlayer.filter(
                                resource_type='lumber').first().delete()
                            resourcePlayer.filter(
                                resource_type='brick').first().delete()
                        else:
                            msg = {
                                'detail': 'you don`t have the necessary' +
                                ' resources to build a route'
                            }
                            return Response(
                                msg, status=status.HTTP_400_BAD_REQUEST)
                    if filteredGame.turn < len(
                            filteredGame.players.all()) and len(
                            player.roads.all()) == 1:
                        msg = {
                            'detail': 'a player can only' +
                            ' build one road in this turn'
                        }
                        return Response(
                            msg, status=status.HTTP_400_BAD_REQUEST)
                    if filteredGame.turn < len(
                            filteredGame.players.all()) * 2 \
                            and len(player.roads.all()) == 2:
                        msg = {
                            'detail': 'a player can only' +
                            ' build one road in this turn'
                        }
                        return Response(
                            msg, status=status.HTTP_400_BAD_REQUEST)
                    RoadPosition.objects.get_or_create(
                        pos1=VertexPosition.objects.get(
                            level=payload[0]['level'],
                            index=payload[0]['index']),
                        pos2=VertexPosition.objects.get(
                            level=payload[1]['level'],
                            index=payload[1]['index']),
                        owner=player)
                    return Response(status=status.HTTP_204_NO_CONTENT)
                else:
                    msg = {
                        'detail': 'there is already a' +
                        ' road in the desired position'
                    }
            return Response(msg, status=status.HTTP_400_BAD_REQUEST)

        elif action == 'play_road_building_card':
            if len(payload) == 2:
                for pay in payload:
                    if len(pay) == 2:
                        for elem in pay:
                            if len(elem) == 2:
                                valid_input(
                                    elem, (elem.get(
                                        'level', None) is None or elem.get(
                                        'index', None) is None))
                            else:
                                msg = 'Invalid input'
                                raise exceptions.ParseError(msg)
                    else:
                        msg = 'Invalid input'
                        raise exceptions.ParseError(msg)
            else:
                msg = 'Invalid input'
                raise exceptions.ParseError(msg)
            vertex1a = payload[0][0]
            vertex1b = payload[0][1]
            vertex2a = payload[1][0]
            vertex2b = payload[1][1]
            if player.card_set.filter(card_type='road_building').exists():
                if is_vertex(vertex1a) and is_vertex(vertex1b) and is_vertex(
                        vertex2a) and is_vertex(vertex2b):
                    if player.build_two_roads(
                            vertex1a=vertex1a,
                            vertex1b=vertex1b,
                            vertex2a=vertex2a,
                            vertex2b=vertex2b,
                            game=filteredGame):
                        player.card_set.filter(
                            card_type='road_building').first().delete()
                    else:
                        if player.build_two_roads(
                                vertex1a=vertex2a,
                                vertex1b=vertex2b,
                                vertex2a=vertex1a,
                                vertex2b=vertex1b,
                                game=filteredGame):
                            player.card_set.filter(
                                card_type='road_building').first().delete()
                        else:
                            msg = {
                                'detail': 'a route position' +
                                ' is not available to be built'
                            }
                            raise exceptions.PermissionDenied(msg)
                else:
                    msg = {'detail': 'the payload not contain a vertex'}
                    raise ParseError(msg)
            else:
                msg = {
                    'detail': 'The current player does not' +
                    ' have a road_bulding_card'
                }
                raise exceptions.PermissionDenied(msg)
            return Response(status=status.HTTP_204_NO_CONTENT)

        elif action == "build_settlement":
            if len(payload) == 2:
                valid_input(payload, (not is_vertex(payload)))
            else:
                msg = 'Invalid input'
                raise exceptions.ParseError(msg)

            game = get_object_or_404(Game, pk=id)
            level, index = payload["level"], payload["index"]

            if BuildTown.check_availability(payload, game):
                vert = get_object_or_404(VertexPosition, level=level,
                                         index=index)

                total_towns = len(player.town_set.all())
                # Then we are in turn >= 2
                if total_towns >= 2:
                    resourcePlayer = resourcePlayer
                    resource_type_list = [x.resource_type for x in
                                          resourcePlayer]
                    road_buy_resource_list = ['ore', 'grain']
                    if resource_type_list.count('ore') >= 3 \
                            and resource_type_list.count('grain') >= 2:
                        res_grain = [
                            resource.pk for resource in
                            resourcePlayer.filter(
                                resource_type='grain'
                            ).all()[:2]
                        ]
                        resourcePlayer.filter(
                            pk__in=list(res_grain)).delete()

                        res_ore = [
                            resource.pk for resource in
                            resourcePlayer.filter(
                                resource_type='ore').all()[:3]
                        ]
                        resourcePlayer.filter(pk__in=list(
                            res_ore)).all().delete()
                    else:
                        return Response(
                            {'detail': 'you don`t have the necessary '
                                + 'resources to build a town'},
                            status=status.HTTP_403_FORBIDDEN
                        )
                if filteredGame.turn < len(
                        filteredGame.players.all()) and len(
                        player.town_set.all()) == 1:
                    msg = {
                        'detail': 'a player can only build' +
                        ' one town in this turn'
                    }
                    return Response(msg, status=status.HTTP_400_BAD_REQUEST)
                if filteredGame.turn < len(
                        filteredGame.players.all()) * 2 \
                        and len(player.town_set.all()) == 2:
                    msg = {
                        'detail': 'a player can only build' +
                        ' one town in this turn'
                    }
                    return Response(msg, status=status.HTTP_400_BAD_REQUEST)
                Town.objects.create(
                    vertex_position=vert,
                    owner=game.in_turn,
                )
            else:
                msg = {'detail': 'it is not a valid location to build a town'}
                return Response(msg, status=status.HTTP_403_FORBIDDEN)
            return Response(status=status.HTTP_204_NO_CONTENT)

        elif action == "play_knight_card":

            if len(payload) == 2:
                valid_input(
                    payload, (payload.get(
                        'position', None) is None or payload.get(
                        'player', None) is None))
                valid_input(
                    payload['position'],
                    (payload['position'].get(
                        'level',
                        None) is None or payload['position'].get(
                        'index',
                        None) is None))
            else:
                msg = 'Invalid input'
                raise exceptions.ParseError(msg)
            return filteredGame.move_robber(payload, action)

        elif action == 'move_robber':

            if len(payload) == 2:
                valid_input(
                    payload, (payload.get(
                        'position', None) is None or payload.get(
                        'player', None) is None))
                valid_input(
                    payload['position'],
                    (payload['position'].get(
                        'level',
                        None) is None or payload['position'].get(
                        'index',
                        None) is None))
            else:
                msg = 'Invalid input'
                raise exceptions.ParseError(msg)
            if (filteredGame.dice1 + filteredGame.dice2) == 7:
                return filteredGame.move_robber(payload, action)
            else:
                msg = 'A 7 must be rolled in order to move the robber'
                raise exceptions.PermissionDenied(msg)

        else:
            msg = 'Invalid action.'
            raise exceptions.ParseError(msg)

    def get(self, request, id):
        if is_game_ended(id):
            return is_game_ended(id)
        try:
            filteredGame = Game.objects.get(id=id)
            playerGame = filteredGame.players.get(user_id=request.user.id)
        except ObjectDoesNotExist:
            raise Http404
        ret = list()
        optionsAux = list()
        lista = list()
        build_settlement_list = get_settlements_available(
            filteredGame
        )
        players = filteredGame.players.all()
        options = playerGame.roads_options(game=filteredGame)
        optionsAux.extend(options)
        ret.extend([{'type': 'buy_card', 'payload': ''}])
        ret.extend([{'type': 'bank_trade', 'payload': ''}])
        ret.extend([{'type': 'end_turn', 'payload': ''}])
        ret.extend([{'type': 'build_road',
                     'payload': optionsAux if (Resource.objects.filter(
                        player=playerGame,
                        resource_type='lumber'
                        ) and Resource.objects.filter(
                        player=playerGame,
                        resource_type='brick') or filteredGame.turn < len(
                                filteredGame.players.all()) * 2) and \
                                playerGame == filteredGame.in_turn else []}])
        ret.extend([{'type': 'play_building_road',
                     'payload': options if Card.objects.filter(
                        player=playerGame,
                        card_type='road_building') else []}])
        ret.extend([{'type': 'play_knight_card',
                    'payload': lista if Card.objects.filter(
                        player=playerGame, card_type='knight') else []}])
        ret.extend([{'type': 'move_robber', 'payload': lista if (
            filteredGame.dice1 + filteredGame.dice2) == 7 else []}])
        ret.extend([{'type': 'build_settlement',
                     'payload': VertexPositionSerializer(
                        build_settlement_list,
                        many=True).data if (len(Resource.objects.filter(
                            player=playerGame,
                            resource_type='ore')) >= 3 and len(
                        Resource.objects.filter(
                            player=playerGame,
                            resource_type='grain'
                            )) >= 2 or filteredGame.turn < len(
                            filteredGame.players.all()) * 2)  and \
                            playerGame == filteredGame.in_turn else []}])

        for i in optionsAux:
            options.extend(playerGame.add_option(
                level=i[0]['level'], index=i[0]['index'], adjacency=adjacency[(
                    i[0]['level'], i[0]['index'])], players=players))
            options.extend(playerGame.add_option(
                level=i[1]['level'], index=i[1]['index'], adjacency=adjacency[(
                    i[1]['level'], i[1]['index'])], players=players))

        for hexa in filteredGame.board.hexagon_set.all():
            players = list()
            position = {}
            if hexa.robber.first() is None:
                position = {
                    "level": hexa.vertex_position.level,
                    "Index": hexa.vertex_position.index}
                for owner_vertices in hexa.vertices.all():
                    for player in filteredGame.players.all():
                        if player.town_set.filter(
                                vertex_position=owner_vertices).first() \
                            is not None \
                            and filteredGame.in_turn.user.username \
                            != player.town_set.filter(
                                vertex_position=owner_vertices).first(
                        ).owner.user.username \
                                and player.town_set.filter(
                                vertex_position=owner_vertices).first(
                        ).owner.user.username not in players:
                            players.append(
                                player.town_set.filter(
                                    vertex_position=owner_vertices).first(
                                ).owner.user.username)
                lista.append({"position": position, "players": players})

        return Response(ret, status=status.HTTP_200_OK)


class GameViewSet(ModelViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return GameListSerializer

    def retrieve(self, request, pk):

        try:
            g = Game.objects.get(id=pk)
        except ObjectDoesNotExist:
            raise Http404

        r_data = {}
        r_data['players'] = []
        for player in g.players.all():
            p_data = {}
            p_data['username'] = player.user.username
            p_data['colour'] = player.colour

            cities = []
            settlements = []
            for town in player.town_set.all():
                if town.is_city:
                    cities.append(
                        VertexPositionSerializer(
                            town.vertex_position).data)
                else:
                    settlements.append(
                        VertexPositionSerializer(
                            town.vertex_position).data)
            p_data['cities'] = cities
            p_data['settlements'] = settlements
            roads = []
            for road in player.roads.all():
                roads.append([VertexPositionSerializer(
                    road.pos1).data, VertexPositionSerializer(road.pos2).data])
            p_data['roads'] = roads

            res = []
            for resources in player.resource_set.all():
                res.append(resources.resource_type)

            card = []
            for cards in player.card_set.all():
                card.append(cards.card_type)

            p_data['development_cards'] = len(card)

            p_data['resources_cards'] = len(res)

            p_data['victory_points'] = 0
            last_gained = []
            for resource in player.resource_set.all():
                if resource.last_gained is not None:
                    last_gained.append(resource.resource_type)
            p_data['last_gained'] = last_gained

            r_data['players'].append(p_data)

        r_data['robber'] = VertexPositionSerializer(
            g.robber.vertex_position
        ).data

        turn = {}
        turn['user'] = g.in_turn.user.username
        turn['dice'] = (g.dice1, g.dice2)
        r_data['current_turn'] = turn
        if g.winner:
            r_data['winner'] = g.winner.user.username

        return Response(r_data)


class BuildTown(APIView):

    @staticmethod
    def check_availability(payload, game):
        game_id = game.pk
        level, index = payload["level"], payload["index"]
        player = game.in_turn
        can_build_neig = check_neighbors(game, level, index)
        if not can_build_neig:
            return False
        return True
