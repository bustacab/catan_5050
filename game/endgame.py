from .models import Game
from rest_framework import status
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404


def is_game_ended(game_id):
    try:
        if Game.objects.get(pk=game_id).winner:
            return Response(
                'This game has already ended',
                status=status.HTTP_403_FORBIDDEN
            )
    except ObjectDoesNotExist:
        raise Http404

    return False
