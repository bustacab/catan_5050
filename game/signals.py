from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from .models import Game, Player, Card, Town, RoadPosition
import operator


def calculate_victory_points(instance):

    game = instance

    if not game or game.winner:
        return

    victory_points = {}
    for player in game.players.all():
        player.victory_points = player.card_set.filter(
            card_type='victory_point'
        ).count() + player.town_set.count()
        player.save()
        if player.victory_points >= 10:
            game.crown(player)


@receiver(post_save, sender=Game)
def count_vp_game(sender, instance, **kwargs):
    calculate_victory_points(instance)


@receiver([post_save, pre_save], sender=Card)
def count_vp_card(sender, instance, **kwargs):
    calculate_victory_points(instance.player.player_game.first())


@receiver([post_save, pre_save], sender=Town)
def count_vp_town(sender, instance, **kwargs):
    calculate_victory_points(instance.owner.player_game.first())
