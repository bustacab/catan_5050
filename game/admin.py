from django.contrib import admin
from game.models import (
    RoadPosition, Card, Resource,
    Game, Player, Town
)


# class GameAdmin(admin.ModelAdmin):
#    exclude = ('turn',)
#
#    class meta:
#        model = Game

admin.site.register(Card)
admin.site.register(Resource)
admin.site.register(Game)
admin.site.register(Player)
admin.site.register(RoadPosition)
admin.site.register(Town)
