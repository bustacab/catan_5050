from django.db import models
from users.models import User
from tablero.models import Board, Hexagon, VertexPosition
from game.adjacency import adjacency
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status, exceptions
from catan.choices import RESOURCES
import random
import math


class Player(models.Model):
    colour = models.CharField(max_length=20)
    user = models.ForeignKey(
        User, to_field='id',
        related_name='users',
        on_delete=models.CASCADE
    )
    turn_number = models.IntegerField(null=True, blank=True)
    victory_points = models.IntegerField(default=0, null=True, blank=True)

    def check_road(self, level1, index1, level2, index2):
        return self.roads.filter(
            pos1=VertexPosition.objects.get(level=level1, index=index1),
            pos2=VertexPosition.objects.get(level=level2, index=index2),
        ).exists()

    def check_my_roads(self, level, index):
        j = 0
        auxbool = False
        for i in adjacency[(level, index)]:
            auxbool = auxbool or self.check_road(
                level1=level, index1=index, level2=i[0], index2=i[1]) or \
                self.check_road(
                    level1=i[0],
                    index1=i[1],
                    level2=level,
                    index2=index
            )
        return auxbool

    def check_my_towns(self, level, index):
        return self.town_set.filter(
            vertex_position=VertexPosition.objects.get(
                level=level, index=index)).exists()

    def get_my_town(self, level, index):
        town = self.town_set.filter(
            vertex_position=VertexPosition.objects.get(
                level=level, index=index
            )
        )
        return town[0] if len(town) > 0 else None

    def get_my_road(self, level1, index1, level2, index2):
        road = self.roads.filter(
            pos1=VertexPosition.objects.get(level=level1, index=index1),
            pos2=VertexPosition.objects.get(level=level2, index=index2),
        )
        return road[0] if len(road) > 0 else None

    def check_availability(self, vertex0, vertex1, game):
        level1, index1 = int(vertex0['level']), int(vertex0['index'])
        level2, index2 = int(vertex1['level']), int(vertex1['index'])
        auxbool = True
        if(level2, index2) in adjacency[(level1, index1)]:
            if not self.check_my_towns(
                    level=level1,
                    index=index1) and not self.check_my_towns(
                    level=level2,
                    index=index2):
                if not self.check_my_roads(
                        level=level1,
                        index=index1) and not self.check_my_roads(
                        level=level2,
                        index=index2):
                    return False
            for i in game.players.all():
                auxbool = auxbool and not i.check_road(
                    level1=level1,
                    index1=index1,
                    level2=level2,
                    index2=index2) and not i.check_road(
                    level1=level2,
                    index1=index2,
                    level2=level1,
                    index2=index1)
            return auxbool
        return False

    def add_option(self, level, index, adjacency, players):
        auxbool = True
        options = list()
        for x in adjacency:
            for j in players:
                auxbool = auxbool and not j.check_road(
                    level1=level, index1=index, level2=x[0], index2=x[1]) and \
                    not j.check_road(
                    level1=x[0],
                    index1=x[1],
                    level2=level,
                    index2=index
                )
            if auxbool:
                options.append([{'level': level, 'index': index}, {
                                'level': x[0], 'index':x[1]}])
            auxbool = True
        return options

    def roads_options(self, game):
        options = list()
        auxbool = True
        my_roads = self.roads.all()
        players = game.players.all()
        my_towns = self.town_set.all()
        for i in my_roads:
            options.extend(self.add_option(level=i.pos1.level,
                                           index=i.pos1.index,
                                           adjacency=adjacency[(i.pos1.level,
                                                                i.pos1.index)],
                                           players=players))
            options.extend(self.add_option(level=i.pos2.level,
                                           index=i.pos2.index,
                                           adjacency=adjacency[(i.pos2.level,
                                                                i.pos2.index)],
                                           players=players))
        for i in my_towns:
            for x in adjacency[(i.vertex_position.level,
                                i.vertex_position.index)]:
                for j in players:
                    auxbool = auxbool and not j.check_road(
                        level1=i.vertex_position.level,
                        index1=i.vertex_position.index,
                        level2=x[0],
                        index2=x[1]) and not j.check_road(
                        level1=x[0],
                        index1=x[1],
                        level2=i.vertex_position.level,
                        index2=i.vertex_position.index)
                if auxbool:
                    options.append([
                        {
                            'level': i.vertex_position.level,
                            'index': i.vertex_position.index},
                        {
                            'level': x[0], 'index':x[1]
                        }
                    ])
                auxbool = True
        return options

    def build_two_roads(self, vertex1a, vertex1b, vertex2a, vertex2b, game):
        if self.check_availability(
                vertex0=vertex1a,
                vertex1=vertex1b,
                game=game):
            road1 = RoadPosition.objects.get_or_create(
                pos1=VertexPosition.objects.get(
                    level=vertex1a['level'],
                    index=vertex1a['index']),
                pos2=VertexPosition.objects.get(
                    level=vertex1b['level'],
                    index=vertex1b['index']),
                owner=self)
            if self.check_availability(
                    vertex0=vertex2a,
                    vertex1=vertex2b,
                    game=game):
                road2 = RoadPosition.objects.get_or_create(
                    pos1=VertexPosition.objects.get(
                        level=vertex2a['level'],
                        index=vertex2a['index']),
                    pos2=VertexPosition.objects.get(
                        level=vertex2b['level'],
                        index=vertex2b['index']),
                    owner=self)
                return True
            else:
                road1[0].delete()
        return False

    def __str__(self):
        return self.user.username

    def __str__(self):
        if self.player_game.all().first():
            return "{} - game {}".format(self.user.username,
                                         self.player_game.all().first().id)
        else:
            return self.user.username


class Game(models.Model):
    name = models.CharField(max_length=50)
    in_turn = models.ForeignKey(
        Player, on_delete=models.CASCADE, null=True, blank=True)
    players = models.ManyToManyField(Player, related_name='player_game')
    dice1 = models.IntegerField(default=0)
    dice2 = models.IntegerField(default=0)
    turn = models.IntegerField(default=0)
    board = models.ForeignKey(
        Board,
        on_delete=models.CASCADE,
        blank=True,
        null=True)
    robber = models.ForeignKey(
        Hexagon,
        related_name='robber',
        on_delete=models.CASCADE,
        null=True)
    winner = models.ForeignKey(
        Player,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='wins'
    )
    robber_is_move = models.BooleanField(default=False)

    def __int__(self):
        return self.id

    def crown(self, player):
        self.winner = player
        self.save()

    def move_robber_and_steal_resources(self, payload, vertex_robber):

        for player_town in self.robber.vertices.all():

            if player_town.town_set.first() is not None and \
                player_town.town_set.first(
            ).owner.user.username != self.in_turn.user.username and \
                    player_town.town_set.first().owner in self.players.all():
                resource = player_town.town_set.first().owner.resource_set.all(
                )
                robbed_resource = resource.order_by("?").first()
                if (payload['player'] is None or payload['player'] != "" or
                    payload['player'] != {} or payload['player'] != []) and \
                        robbed_resource is not None:
                    robbed_resource.player = self.in_turn
                    robbed_resource.save()
                elif (payload['player'] == player_town.town_set.first(
                ).owner.user.username) and robbed_resource is not None:
                    robbed_resource.player = self.in_turn
                    robbed_resource.save()
                self.robber_is_move = True
                self.save()

    def delete_card(self, card):
        self.in_turn.card_set.filter(card_type=card).first().delete()

    def move_robber(self, payload, action):
        try:
            vertex_robber = Hexagon.objects.get(
                vertex_position=VertexPosition.objects.get(
                    level=payload['position']['level'],
                    index=payload['position']['index']))
        except ObjectDoesNotExist:
            msg = "Vertex does not exists"
            raise exceptions.PermissionDenied(msg)

        if not self.in_turn.card_set.filter(
                card_type='knight') and action == "play_knight_card":
            msg = "A player must have a card knight"
            raise exceptions.PermissionDenied(msg)

        if self.robber_is_move:
            msg = "A player can move the robber only once in this turn"
            raise exceptions.PermissionDenied(msg)

        if (len(set([player_town.town_set.first().owner
                     for player_town in vertex_robber.vertices.all()
                     if player_town.town_set.first() is not None
                     and player_town.town_set.first().owner
                     in self.players.all()
                     and self.in_turn != player_town.town_set.first().owner]))
                > 1):
            msg = "too many players to steal, player must choose only one"
            raise exceptions.PermissionDenied(msg)
        if self.robber != vertex_robber:
            self.robber = vertex_robber
            self.save()
        else:
            msg = "Invalid position for robber"
            raise exceptions.PermissionDenied(msg)
        if payload['player'] is None or payload['player'] == ""\
                or payload['player'] == {} or payload['player'] == []:
            if (len(set([player_town.town_set.first().owner
                        for player_town in vertex_robber.vertices.all()
                        if player_town.town_set.first() is not None
                        and player_town.town_set.first().owner
                        in self.players.all()
                        and self.in_turn != player_town.town_set.first().owner]
                        )) == 1):
                self.move_robber_and_steal_resources(payload, vertex_robber)
                if action == "play_knight_card":
                    self.delete_card("knight")
                return Response(status=status.HTTP_204_NO_CONTENT)

        elif payload['player'] not in [player_town.town_set.first(
        ).owner.user.username
            for player_town in vertex_robber.vertices.all()
                if player_town.town_set.first() is not None
                and player_town.town_set.first().owner.resource_set.all()]:
            msg = "Invalid player to steal"
            raise exceptions.PermissionDenied(msg)
        elif payload['player'] == self.in_turn.user.username:
            msg = "players can't steal themselves"
            raise exceptions.PermissionDenied(msg)
        else:
            self.move_robber_and_steal_resources(payload, vertex_robber)
            if action == "play_knight_card":
                self.delete_card("knight")
            return Response(status=status.HTTP_204_NO_CONTENT)
        if action == "play_knight_card":
            self.delete_card("knight")
        self.robber_is_move = True
        self.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def player_resources(self, user):
        get_player = self.players.get(user=user)
        get_player_resources = Resource.objects.filter(player=get_player)
        return get_player_resources

    def player_cards(self, user):
        get_player = self.players.get(user=user)
        get_player_cards = Card.objects.filter(player=get_player)
        return get_player_cards

    def end_turn(self):
        self.robber_is_move = False
        amount_players = len(self.players.all())
        round1 = list(range(0, amount_players))
        round2 = round1[::-1]
        turn_list = round1 + round2
        turn = self.turn
        if turn < 2 * amount_players:
            msg = "Players cannot end turn without building" +\
                " one town and one road during this turn"
            if turn < amount_players \
                    and ((len(self.in_turn.town_set.all()) != 1) or (
                        len(self.in_turn.roads.all()) != 1)):
                raise exceptions.PermissionDenied(msg)
            elif turn >= amount_players \
                and ((len(self.in_turn.town_set.all()) != 2) or (
                    len(self.in_turn.roads.all()) != 2)):
                raise exceptions.PermissionDenied(msg)
            if turn + 1 != 2 * amount_players:
                next_turn = turn_list[turn + 1]
                if next_turn < 2 * amount_players:
                    self.in_turn = self.players.get(turn_number=next_turn)
        else:
            next_turn = turn_list[(turn + 1) % amount_players]
            self.in_turn = self.players.get(turn_number=next_turn)
        # pass turn
        self.turn += 1
        # saves the game status
        self.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def roll_dices(self):
        self.dice1 = random.randrange(1, 7, 1)
        self.dice2 = random.randrange(1, 7, 1)
        # check 7
        result = self.dice1 + self.dice2
        if result == 7:
            for pno in self.players.all():
                playerResources = Resource.objects.filter(
                    player=pno)
                resourcesCount = len(playerResources)
                if (resourcesCount > 7):
                    for x in range(math.trunc(resourcesCount / 2)):
                        playerResources.order_by("?").first().delete()
        return result

    def bank_trade(self, payload, resourcePlayer):
        resource_list = ['ore', 'wool', 'grain', 'lumber', 'brick']
        payload_check_give = payload['give'] not in resource_list \
            if payload else False
        payload_check_receive = payload['receive'] not in resource_list \
            if payload else False
        if payload_check_give or payload_check_receive and action \
                in valid_actions:
            msg = 'Input arguments are not resources.'
            raise exceptions.ParseError(msg)
        resourcePlayer = resourcePlayer
        if len(resourcePlayer.filter(resource_type=payload['give'])) >= 4:

            if payload['receive'] == payload['give']:
                msg = "You cannot trade with the same resources."
                raise exceptions.PermissionDenied(msg)

            count = 4
            for res in resourcePlayer:
                if count > 0 and res.resource_type == payload['give']:
                    resourcePlayer.filter(
                        resource_type=payload['give']).first().delete()
                    count = count - 1

            resourceBank = Resource.objects.create(
                player=self.in_turn, resource_type=payload['receive'], )
            return Response(status=status.HTTP_200_OK)
        else:
            msg = "Insufficient resources to complete the trade."
            raise exceptions.PermissionDenied(msg)

    def buy_card(self, resourcePlayer):
        resourcePlayer = resourcePlayer
        resource_type_list = [x.resource_type for x in resourcePlayer]
        card_buy_resource_list = ['ore', 'wool', 'grain']
        cards_list = ['road_building', 'year_of_plenty',
                      'monopoly', 'victory_point', 'knight']
        if set(card_buy_resource_list).issubset(resource_type_list):
            resourcePlayer.filter(resource_type='ore').first().delete()
            resourcePlayer.filter(resource_type='wool').first().delete()
            resourcePlayer.filter(resource_type='grain').first().delete()
            dev_card = Card.objects.create(player=self.in_turn,
                                           card_type=cards_list[
                                               random.randrange(0, 5, 1)
                                           ])
            return Response(status=status.HTTP_200_OK)
        else:
            msg = "Insufficient resources to buy a card."
            raise exceptions.PermissionDenied(msg)

    def distribute_resources(self, result):
        players_in_game = self.players.all()
        players_list = [player for player in players_in_game]
        game_resource_list = [
            resource for player in players_list
            for resource in player.resource_set.all()]
        for resource in game_resource_list:
            resource.last_gained = None
            resource.save()
        hexagon = Hexagon.objects.filter(token=result)
        for hexa in hexagon:
            for vertex in hexa.vertices.all():
                for town in Town.objects.all():
                    if town.vertex_position == vertex \
                        and town.owner in players_in_game \
                        and self.robber.vertex_position \
                            != hexa.vertex_position:
                        resource = Resource.objects.create(
                            player=town.owner, resource_type=hexa.resource)
                        resource.last_gained = town.owner
                        resource.save()

    def __init__(self, *args, **kwargs):
        super(Game, self).__init__(*args, **kwargs)
        self.__previous_turn = self.in_turn

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        if self.id is not None:
            amount_of_players = len(self.players.all())
            if self.turn == 2 * amount_of_players:
                result = self.roll_dices()
                self.distribute_resources(result)
            elif self.in_turn != self.__previous_turn \
                    and self.turn > 2 * amount_of_players:
                result = self.roll_dices()
                self.distribute_resources(result)
        super(Game, self).save(force_insert, force_update, *args, **kwargs)
        self.__previous_turn = self.in_turn


class RoadPosition(models.Model):
    pos1 = models.ForeignKey(
        VertexPosition,
        on_delete=models.SET_NULL, null=True,
        related_name='firstpos'
    )
    pos2 = models.ForeignKey(
        VertexPosition,
        on_delete=models.SET_NULL, null=True,
        related_name='secondpos'
    )

    owner = models.ForeignKey(
        Player,
        blank=True,
        on_delete=models.SET_NULL, null=True,
        related_name='roads'
    )

    def __str__(self):
        return "{} - {}".format(self.pos1, self.pos2)

    class Meta:
        unique_together = (('pos1', 'pos2', 'owner'),)


class Town(models.Model):
    owner = models.ForeignKey(Player, on_delete=models.CASCADE)
    vertex_position = models.ForeignKey(
        VertexPosition, on_delete=models.CASCADE)
    is_city = models.BooleanField(default=False)


class Resource(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    resource_type = models.CharField(max_length=15, choices=RESOURCES)
    last_gained = models.ForeignKey(
        Player,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='last_gained'
    )

    def __str__(self):
        return self.resource_type


class Card(models.Model):
    CARDS = (
        ('road_building', 'road_building'),
        ('year_of_plenty', 'year_of_plenty'),
        ('monopoly', 'monopoly'),
        ('victory_point', 'victory_point'),
        ('knight', 'knight'),
    )
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    card_type = models.CharField(max_length=15, choices=CARDS)

    def __str__(self):
        return self.card_type
