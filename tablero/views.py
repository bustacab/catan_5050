from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView, Response
from tablero.models import (Hexagon, Board, VertexPosition)
from django.core.exceptions import ObjectDoesNotExist
from game.models import Game
from .serializers import (
    HexagonSerializer,
    BoardSerializer,
    VertexPositionSerializer
)
from game.endgame import is_game_ended


class VertexPositionCRUD(ModelViewSet):
    queryset = VertexPosition.objects.all()
    serializer_class = VertexPositionSerializer


class BoardCRUD(ModelViewSet):
    queryset = Board.objects.all()
    serializer_class = BoardSerializer


class HexagonView(APIView):

    def get(self, request, id):
        if is_game_ended(id):
            return is_game_ended(id)
        try:
            game = Game.objects.get(pk=id)
            board = Board.objects.get(pk=game.board_id)
        except ObjectDoesNotExist:
            return Response(
                "Game does not exist",
                status=404
            )
        return Response(
            {"hexes": HexagonSerializer(
                board.hexagon_set.all(),
                many=True
            ).data}
        )
