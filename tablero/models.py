from django.db import models
from catan.choices import TERRAIN


class Board(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class VertexPosition(models.Model):
    level = models.PositiveSmallIntegerField()
    index = models.PositiveSmallIntegerField()

    def __str__(self):
        return "{} - {}".format(self.level, self.index)

    class Meta:
        unique_together = (('level', 'index'),)


class Hexagon(models.Model):
    resource = models.CharField(max_length=10, choices=TERRAIN)
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    vertex_position = models.OneToOneField(
        VertexPosition, on_delete=models.SET_NULL, null=True)
    vertices = models.ManyToManyField(VertexPosition, related_name="vertices")
    token = models.IntegerField(default=0)

    def __str__(self):
        return "{} - {}".format(self.vertex_position, self.resource)

    def get_terrain(self):
        return str(self.resource)
        
    def get_position(self):
        return self.vertex_position

