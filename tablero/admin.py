from django.contrib import admin
from tablero.models import (Board,
                            Hexagon, VertexPosition)

admin.site.register(Board)
admin.site.register(Hexagon)
admin.site.register(VertexPosition)
