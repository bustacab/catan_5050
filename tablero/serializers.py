from rest_framework import serializers
from tablero.models import (Hexagon,
                            Board,
                            VertexPosition
                            )


class BoardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Board
        fields = "__all__"


class VertexPositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = VertexPosition
        fields = ['level', 'index']


class HexagonSerializer(serializers.ModelSerializer):
    position = VertexPositionSerializer(source="get_position")
    terrain = serializers.ReadOnlyField(source="get_terrain")
    class Meta:
        model = Hexagon
        fields = ['position', 'terrain', 'token']
