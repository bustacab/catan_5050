from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient
from users.models import User
import random
from tablero.models import *
from game.models import *
from catan.choices import TERRAIN
import json


class BoardTestCase(APITestCase):

    def setUp(self):
        self.user1 = User.objects.create_user(
            username='P245', password='Djangotest1')
        self.token = Token.objects.create(user=self.user1)
        self.client = APIClient()

        self.player1 = Player.objects.create(colour='green',
                                             user=self.user1
                                             )

        self.board = Board.objects.create(
            pk=1,
            name="Tablero Ejemplo"
        )

        self.game = Game.objects.create(id=1,
                                        name='gam',
                                        in_turn=self.player1,
                                        board=self.board
                                        )

        self.Terrain = ['brick', 'ore', 'lumber', 'wool', 'grain', 'desert']

        for level in range(3):
            if level == 0:
                r = 1
            elif level == 1:
                r = 6
            else:
                r = 12
            for index in range(r):
                if level == 0 and index == 0:
                    resource = 'desert'
                else:
                    resource = self.Terrain[
                        random.randint(0,
                                       len(self.Terrain) - 1)
                    ]
                ver = VertexPosition.objects.get_or_create(
                    level=level,
                    index=index
                )
                hexa = Hexagon.objects.get_or_create(
                    resource=resource,
                    board=self.board,
                    vertex_position=ver[0],
                    token=random.randint(2, 12)
                )

    def test_get_tablero(self):
        response = self.client.get(
            reverse(
                'game-id-board',
                kwargs={
                    'id': 1}),
            format='json',
            HTTP_AUTHORIZATION='Token ' +
            self.token.key)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_token_no_valid(self):
        response = self.client.get(
            reverse(
                'game-id-board',
                kwargs={
                    'id': 1}),
            format='json',
            HTTP_AUTHORIZATION='Token ' +
            "somestringrandom")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
