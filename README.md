# Requisitos para instalación(soportado solamente por Ubuntu)

## Instalar pip — Ubuntu

```shell
sudo apt-get update
```

```shell
sudo apt-get install python-pip
```

```shell
sudo apt-get install python3-pip
```

## Instalar virtualenv

```shell
pip install virtualenv
```
```shell
sudo pip3 install virtualenv
```

# Instalar la API

### Descargar el repositorio

```shell
git clone https://gitlab.com/bustacab/catan_5050.git
```

### Crear un entorno virtual

```shell
virtualenv nombre_de_tu_entorno -p python3
```

Esto creara una nueva carpeta dentro del directorio en el que estemos parados, allí se instalaran todos los paquetes que desees utilizar.

### Activar / Desactivar entorno virtual

Para ello deberás estar sobre el directorio en el cual creamos el entorno virtual y ejecutar el siguiente comando.

```shell
source nombre_de_tu_entorno/bin/activate
```

**Una ves que ya no necesitamos del entorno virtual podemos desactivarlo con el siguiente comando:**

```shell
deactivate
```

### Correr el Servidor 

Ingresar a la carpeta que contiene el repositorio

```shell
cd catan_5050
```
Los requerimientos utilizados para correr el servidor son los siguientes:
```
autopep8==1.4.4
backcall==0.1.0
colorama==0.4.1
coverage==4.5.4
decorator==4.4.0
Django==2.2.6
django-extensions==2.2.3
django-shell-plus==1.1.7
djangorestframework==3.10.3
ipdb==0.12.2
ipython==7.8.0
ipython-genutils==0.2.0
jedi==0.15.1
parso==0.5.1
pep8==1.7.1
pexpect==4.7.0
pickleshare==0.7.5
prompt-toolkit==2.0.9
ptyprocess==0.6.0
pycodestyle==2.5.0
pydot==1.4.1
Pygments==2.4.2
pyparsing==2.4.2
pytz==2019.2
six==1.12.0
sqlparse==0.3.0
traitlets==4.3.2
wcwidth==0.1.7
```
Dichos requerimientos serán instalados ejecutando la siguiente linea:

```shell
pip install -r requirements.txt
```

luego ejecutar las siguientes lineas

```shell
python manage.py makemigrations
```
```shell
python manage.py migrate
```
### Cargar tablero

```shell
python manage.py runscript Board.py -v3
```

y por ultimo 

```shell
python manage.py runserver
```

nos aparecerá los siguiente 

```shell
Watching for file changes with StatReloader
Performing system checks...

System check identified no issues (0 silenced).
October 09, 2019 - 21:03:57
Django version 2.2.6, using settings 'catan.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CONTROL-C.
```

esto nos mostrara en donde se esta corriendo en servidor en nuestro caso http://127.0.0.1:8000/

## Tests

Para poder correr los test deberemos ejecutar la siguiente linea de comando dentro de la carpeta catan_5050, sin estar corriendo el servidor y estando activado el entorno virtual.

```shell
python manage.py test
```

esto nos devolverá algo similar si todo anduvo bien.

```shell
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
..........
----------------------------------------------------------------------
Ran 10 tests in 3.199s

OK
Destroying test database for alias 'default'...

```

### Integración con frontend en versión actual
En la versión presente del código el funcionamiento actual abarca los siguientes procedimientos funcionales:
* Register
* Login
* Logout
* Create lobby
* Get lobbies
* Join lobby
* Start game
* Cancel lobby
* Get actions
* Get player info
* Get boards

Las posibles acciones a realizar en el juego no pueden ser ejecutadas debido a que los botones correspondientes a dichas acciones no se muestran. 
Por otro lado la gran parte de las modificaciones en la base de datos pueden visualizarse en el tablero, recursos y/o cartas en el juego.
Además los colores de las construcciones se muestran en negro independientemente del jugador.

### Autores
* [Facundo Quiroga](https://gitlab.com/P248)
* [Francisco Griguol](https://gitlab.com/Griwold)
* [Martín Lusso](https://gitlab.com/j.martinlusso)
* [Santiago Bustamante](https://gitlab.com/bustacab)
* [Nahuel Borda](https://gitlab.com/purobardo)

