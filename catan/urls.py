"""catan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import path, include
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from tablero.views import BoardCRUD, VertexPositionCRUD, HexagonView

router = DefaultRouter()
router.register(r'boards', BoardCRUD)
router.register(r'vertex_position', VertexPositionCRUD)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/', include('users.urls')),
    path('games/', include('game.urls', namespace='game-id')),
    path('games/<int:id>/board', HexagonView.as_view(),
         name='game-id-board'),
    path('rooms/', include('room.urls'))
] + router.urls
