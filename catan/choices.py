
from rest_framework import exceptions

RESOURCES = (
    ("brick", "brick"),
    ("lumber", "lumber"),
    ("wool", "wool"),
    ("grain", "grain"),
    ("ore", "ore")
)

TERRAIN = (
    ("desert", "desert"),
    ("Resources", RESOURCES)
)


def valid_input(payload, logic):
    for pay in payload:
        if logic:
            msg = 'Invalid input'
            raise exceptions.ParseError(msg)
